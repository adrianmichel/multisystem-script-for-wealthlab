(*Description...
MultiSystem V1.1 is a framework for integrating different scripts and running them as one system.

This script contains the Position class and its methods definitions.

This script is not a trading system in itself, but rather a tool for developing more sophisticated  trading systems. MultiSystem compatible scripts cannot be run on the web site, as the include directive {$I} is only supported by the Wealth-Lab Developer, so you will need to download all the components on the desktop and run them from there.

Click <b><a href="http://home.pacbell.net/michel/multisystem_v1_1_help.htm">here</a></b> for the complete documentation.

These are the different components of the MultiSystem set of scripts:
<b>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18706">MultiSystem V1.1</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18731">MultiSystem V1.1 Positions</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18708">MultiSystem V1.1 FX</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18710">MultiSystem V1.1 MasterSample</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18711">MultiSystem V1.1 Sample System1</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18712">MultiSystem V1.1 Sample System2</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18713">MultiSystem V1.1 Sample System3</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18714">MultiSystem V1.1 System Template</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18715">MultiSystem V1.1 Filtered System</a>
</b>

The sample provided also uses these utility scripts, so they will have to be present in the system in order for the sample to run without errors:

<b><a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18716">File</a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=18717">SymbolsList</a>
</b>

Here are links to the previous versions of the MultiSystem set of scripts
<b><a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=14774">MultiSystem V1.0 </a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=14912">MultiSystem V1.0.1 </a>
<a href="http://www.wealth-lab.com/cgi-bin/WealthLab.DLL/editsystem?id=15040">MultiSystem V1.0.2 </a></b>

*)
//***********************************
// MultiSystem V1.1
//
// Positions - Position class
//
// (C) Adrian Michel
//***********************************

//***********************************
// Positions class
//
// most important class in this script - it implements all the redefined
// WealthScript functions + the lists of positions (_pos : all positions, _activePos : only active )
// it also stores the parameters used by different Install... and ApplyAutoStop methods.
//***********************************
type Positions = class
  private _pos : TList;
  private _activePos : ActivePos;

  _breakEvenStop : SettableFloat;
  _profitTarget : SettableFloat;
  _reverseBreakEvenStop : SettableFloat;
  _stopLoss : SettableFloat;
  _timeBasedExit : SettableInt;
  _trailingStop : Settable2Float;
  _positionSizeContext : PositionSizeContext;
  _positionNameExtension : StringExtension;

  protected
    constructor Create;
    procedure Free;

    procedure addPosition;
    procedure removePosition( bar : integer; index : integer );
    procedure ApplyTimeBased( Bar : integer; pos : integer );
    procedure ApplyStopLoss( Bar : integer; pos : integer );
    procedure ApplyTrailing( Bar, pos, index : integer );
    procedure ApplyBreakEven( Bar, pos, index : integer );
    procedure ApplyReverseBreakEven( Bar, pos, index : integer );
    procedure ApplyProfitTarget( Bar : integer; pos : integer );

  protected
    function ActivePositionCount : integer;
    function GetPositionData( Position : integer ) : float;
    function GetPositionRiskStop( Position : integer ) : float;
    function LastActivePosition : integer;
    function LastPosition : integer;
    function LastPositionActive : boolean;
    function MarketPosition : integer;
    function PositionActive( Position : integer ) : boolean;
    function PositionCount: integer;

    function PositionEntryBar( Position: integer ): integer;
    function PositionEntryPrice( Position: integer ): float;
    function PositionExitBar( Position: integer ): integer;
    function PositionExitPrice( Position: integer ): float;
    function PositionLong( Position: integer ): boolean;
    function PositionMAE( Position: integer ): float;
    function PositionMAEPct( Position: integer ): float;
    function PositionMFE( Position: integer ): float;
    function PositionMFEPct( Position: integer ): float;
    function PositionOpenMAE( Position, Bar: integer ): float;
    function PositionOpenMAEPct( Position, Bar: integer ): float;
    function PositionOpenMFE( Position, Bar: integer ): float;
    function PositionOpenMFEPct( Position, Bar: integer ): float;
    function PositionOpenProfit( Bar, Position: integer ): float;
    function PositionOpenProfitPct( Bar, Position: integer ): float;
    function PositionProfit( Position: integer ): float;
    function PositionProfitPct( Position: integer ): float;
    function PositionShares( Position: integer ): integer;
    function PositionShort( Position: integer ): boolean;
    function PositionSignalName( Position: integer ): string;
    function PositionSymbol( Position : integer ): string;
    procedure SetPositionData( Position: integer; Value: float );
    procedure SetPositionRiskStop( Position: integer; StopLevel: float );

  protected
    function BuyAtClose( Bar : integer; SignalName : string ) : boolean;
    function BuyAtLimit( Bar : integer; LimitPrice : float; SignalName : string ) : boolean;
    function BuyAtMarket( Bar : integer; SignalName : string ): boolean;
    function BuyAtStop( Bar : integer; StopPrice : float; SignalName : string ): boolean;

    function ShortAtClose( Bar : integer; SignalName : string ) : boolean;
    function ShortAtLimit( Bar: integer; LimitPrice: float; SignalName: string ): boolean;
    function ShortAtMarket( Bar: integer; SignalName: string ) : boolean;
    function ShortAtStop( Bar: integer; StopPrice: float; SignalName: string ): boolean;

    function CoverAtClose( Bar, Position : integer; SignalName : string ) : boolean;
    function CoverAtLimit( Bar : integer; LimitPrice : float; Position: integer; SignalName : string ) : boolean;
    function CoverAtMarket( Bar, Position : integer; SignalName : string ) : boolean;
    function CoverAtStop( Bar : integer; StopPrice: float; Position : integer; SignalName : string ) : boolean;

    function SellAtClose( Bar, Position : integer; SignalName : string ) : boolean;
    function SellAtLimit( Bar : integer; LimitPrice : float; Position : integer; SignalName : string ) : boolean;
    function SellAtMarket( Bar, Position : integer; SignalName : string ) : boolean;
    function SellAtStop( Bar: integer; StopPrice: float; Position: integer; SignalName: string ): boolean;

    function sellAtTrailingStop( Bar : integer; Stop : float; Position : integer; SignalName : string ) : boolean;
    function coverAtTrailingStop( Bar : integer; Stop : float; Position : integer; SignalName : string ) : boolean;

    function buyAtCloseFilter( Bar : integer; SignalName : string ) : boolean; virtual;
    function buyAtLimitFilter( Bar : integer; LimitPrice : float; SignalName : string ) : boolean; virtual;
    function buyAtMarketFilter( Bar : integer; SignalName : string ): boolean; virtual;
    function buyAtStopFilter( Bar : integer; StopPrice : float; SignalName : string ): boolean; virtual;

    function shortAtCloseFilter( Bar : integer; SignalName : string ) : boolean; virtual;
    function shortAtLimitFilter( Bar: integer; LimitPrice: float; SignalName: string ): boolean; virtual;
    function shortAtMarketFilter( Bar: integer; SignalName: string ) : boolean; virtual;
    function shortAtStopFilter( Bar: integer; StopPrice: float; SignalName: string ): boolean; virtual;

    function coverAtCloseFilter( Bar, Position : integer; SignalName : string ) : boolean; virtual;
    function coverAtLimitFilter( Bar : integer; LimitPrice : float; Position: integer; SignalName : string ) : boolean; virtual;
    function coverAtMarketFilter( Bar, Position : integer; SignalName : string ) : boolean; virtual;
    function coverAtStopFilter( Bar : integer; StopPrice: float; Position : integer; SignalName : string ) : boolean;virtual;

    function sellAtCloseFilter( Bar, Position : integer; SignalName : string ) : boolean; virtual;
    function sellAtLimitFilter( Bar : integer; LimitPrice : float; Position : integer; SignalName : string ) : boolean; virtual;
    function sellAtMarketFilter( Bar, Position : integer; SignalName : string ) : boolean; virtual;
    function sellAtStopFilter( Bar: integer; StopPrice: float; Position: integer; SignalName: string ): boolean; virtual;

    function sellAtTrailingStopFilter( Bar: integer; Stop: float; Position: integer; SignalName: string ): boolean; virtual;
    function coverAtTrailingStopFilter( Bar: integer; Stop: float; Position: integer; SignalName: string ): boolean; virtual;

    procedure ApplyAutoStops( Bar : integer );
    procedure InstallBreakEvenStop( Trigger : float );
    procedure InstallProfitTarget( TargetLevel : float );
    procedure InstallReverseBreakEvenStop( LossLevel : float );
    procedure InstallStopLoss( StopLevel : float );
    procedure InstallTimeBasedExit( Bars : integer );
    procedure InstallTrailingStop( Trigger, StopLevel : float );

    function SplitPosition( Position: integer; RetainPct: float ): integer;
    procedure SetPositionSize( Size : float );

    //SimuScript methods
  protected
    function PositionBasisPrice( Position: integer ): float;
    procedure SetPositionSizeFixed( Value: float );
    procedure SetPositionSizePct( Value: float );
    procedure SetPositionSizeShares( Value: integer );

    //
  public procedure switchContext;

    // convenience methods for adjusting the trade name
  protected
    procedure resetAppendPrependName;
    procedure appendStrToTradeName( str : String );
    procedure prependStrToTradeName( str : String );

    // transforming system position number to global WL position number
  public
    function getPositionGlobalIndex( index : integer ) : integer;
    function getActivePositions : ActivePos;

end;

//***********************************
// MultiSystem V1.1
//
// Positions.impl - implementations of Positions class methods
//
// Adrian Michel 2003
//***********************************

function Positions.getActivePositions : ActivePos;
begin
  Result := _activePos;
end;

function Positions.buyAtCloseFilter( Bar : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.buyAtLimitFilter( Bar : integer; LimitPrice : float; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.buyAtMarketFilter( Bar : integer; SignalName : string ): boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.buyAtStopFilter( Bar : integer; StopPrice : float; SignalName : string ): boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.shortAtCloseFilter( Bar : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.shortAtLimitFilter( Bar: integer; LimitPrice: float; SignalName: string ): boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.shortAtMarketFilter( Bar: integer; SignalName: string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.shortAtStopFilter( Bar: integer; StopPrice: float; SignalName: string ): boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.coverAtCloseFilter( Bar, Position : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.coverAtLimitFilter( Bar : integer; LimitPrice : float; Position: integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.coverAtMarketFilter( Bar, Position : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.coverAtStopFilter( Bar : integer; StopPrice: float; Position : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.coverAtTrailingStopFilter( Bar : integer; Stop: float; Position : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.sellAtCloseFilter( Bar, Position : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.sellAtLimitFilter( Bar : integer; LimitPrice : float; Position : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.sellAtMarketFilter( Bar, Position : integer; SignalName : string ) : boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.sellAtStopFilter( Bar: integer; StopPrice: float; Position: integer; SignalName: string ): boolean;
begin
  // by default no filtering
  Result := true;
end;

function Positions.sellAtTrailingStopFilter( Bar: integer; Stop: float; Position: integer; SignalName: string ): boolean;
begin
  // by default no filtering
  Result := true;
end;

procedure Positions.resetAppendPrependName;
begin
  _positionNameExtension.reset;
end;

procedure Positions.appendStrToTradeName( str : String );
begin
  _positionNameExtension.append( str );
end;

procedure Positions.prependStrToTradeName( str : String );
begin
  _positionNameExtension.prepend( str );
end;

constructor Positions.Create;
begin
  _pos := TList.Create;
  _activePos := ActivePos.Create;

  _breakEvenStop := SettableFloat.Create;
  _profitTarget := SettableFloat.Create;
  _reverseBreakEvenStop := SettableFloat.Create;
  _stopLoss := SettableFloat.Create;
  _timeBasedExit := SettableInt.Create;
  _trailingStop := Settable2Float.Create;
  _positionSizeContext := PositionSizeContext.Create;
  _positionNameExtension := StringExtension.Create;
end;

procedure Positions.Free;
begin
  _pos.Free;
end;

function Positions.getPositionGlobalIndex( index : integer ) : integer;
begin
  if( index >= 0 ) then
    //TODO: see what happens if index is not in list
    Result := _pos.Item( index )
  else
    Result := index;
end;

function Positions.ActivePositionCount : integer;
begin
  Result := _activePos.count;
end;

function Positions.GetPositionData( Position : integer ) : float;
begin
  Result := GetPositionDataX( getPositionGlobalIndex( Position ) );
end;

function Positions.GetPositionRiskStop( Position : integer ) : float;
begin
  Result := GetPositionRiskStopX( getPositionGlobalIndex( Position ) );
end;

// last active position
function Positions.LastActivePosition : integer;
begin
  Result := _activePos.last;
end;

// last created position
function Positions.LastPosition : integer;
begin
  Result := _pos.Count - 1
end;

// true if last position is active
function Positions.LastPositionActive : boolean;
begin
  if( _activePos.last < 0 ) then
    Result := false
  else
    Result := _activePos.last = LastPosition;
end;

// returns 0 if no open positions, 1 if last position is long, -1 if last position is short
function Positions.MarketPosition : integer;
begin
  if _activePos.count = 0 then
    Result := 0
  else if PositionLong( LastPosition ) then
    Result := 1
  else
    Result := -1;
end;

function Positions.PositionActive( Position : integer ) : boolean;
begin
  Result := PositionActiveX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionCount: integer;
begin
  Result := _pos.Count();
end;

function Positions.PositionEntryBar( Position: integer ): integer;
begin
  Result := PositionEntryBarX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionEntryPrice( Position: integer ): float;
begin
  Result := PositionEntryPriceX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionExitBar( Position: integer ): integer;
begin
  Result := PositionExitBarX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionExitPrice( Position: integer ): float;
begin
  Result := PositionExitPriceX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionLong( Position: integer ): boolean;
begin
  Result := PositionLongX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionMAE( Position: integer ): float;
begin
  Result := PositionMAEX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionMAEPct( Position: integer ): float;
begin
  Result := PositionMAEPctX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionMFE( Position: integer ): float;
begin
  Result := PositionMFEX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionMFEPct( Position: integer ): float;
begin
  Result := PositionMFEPctX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionOpenMAE( Position, Bar: integer ): float;
begin
  Result := PositionOpenMAEX( getPositionGlobalIndex( Position) , Bar );
end;

function Positions.PositionOpenMAEPct( Position, Bar: integer ): float;
begin
  Result := PositionOpenMAEPctX( getPositionGlobalIndex( Position ), Bar );
end;

function Positions.PositionOpenMFE( Position, Bar: integer ): float;
begin
  Result := PositionOpenMFEX( getPositionGlobalIndex( Position ), Bar );
end;

function Positions.PositionOpenMFEPct( Position, Bar: integer ): float;
begin
  Result := PositionOpenMFEPctX( getPositionGlobalIndex( Position ), Bar );
end;

function Positions.PositionOpenProfit( Bar, Position: integer ): float;
begin
  Result := PositionOpenProfitX( Bar, getPositionGlobalIndex( Position ) );
end;

function Positions.PositionOpenProfitPct( Bar, Position: integer ): float;
begin
  Result := PositionOpenProfitPctX( Bar, getPositionGlobalIndex( Position ) );
end;

function Positions.PositionProfit( Position: integer ): float;
begin
  Result := PositionProfitX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionProfitPct( Position: integer ): float;
begin
  Result := PositionProfitPctX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionShares( Position: integer ): integer;
begin
  Result := PositionSharesX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionShort( Position: integer ): boolean;
begin
  Result := PositionShortX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionSignalName( Position: integer ): string;
begin
  Result := PositionSignalNameX( getPositionGlobalIndex( Position ) );
end;

function Positions.PositionSymbol( Position : integer ): string;
begin
  Result := PositionSymbolX( getPositionGlobalIndex( Position ) );
end;

procedure Positions.SetPositionData( Position: integer; Value: float );
begin
  SetPositionDataX( getPositionGlobalIndex( Position ), Value );
end;

procedure Positions.SetPositionRiskStop( Position: integer; StopLevel: float );
begin
  SetPositionRiskStopX( getPositionGlobalIndex( Position ), StopLevel );
end;

procedure Positions.ApplyTrailing( Bar, pos, index : integer );
begin
  var level : float;
  level := _activePos.getTrailingStopLevel( index );
  if( PositionLong( pos ) ) then
  begin
    if( _activePos.isTrailingStopActivated( index ) ) then
    begin
      var stop : float;
      stop := level - ( level - PositionEntryPrice( pos ) ) * _trailingStop.get2/100 ;
      if( bar <> PositionEntryBar( pos ) ) then
      begin
        if( not SellAtStop( Bar, stop, pos, 'Trailing Stop' ) ) then
        begin
        var newLevel : float;
        newLevel := Max( PriceClose( Bar ), _activePos.getTrailingStopLevel( index ) );
        _activePos.setTrailingStopLevel( index, newLevel );
        end
      end;

      if( PositionActive( pos ) and  ( bar = barcount - 1 ) ) then
        SellAtStop( Bar + 1, stop, pos, 'Trailing Stop' );

    end
    else if( ( bar <> PositionEntryBar(pos ) ) and ( PriceClose( Bar ) >= ( PositionEntryPrice( pos ) * ( 1 + _trailingStop.get1/100 ) ) ) ) then
    begin
      _activePos.setTrailingStopLevel( index, PriceClose( Bar ) );
    end
  end
  else
  begin
    if( _activePos.isTrailingStopActivated( index ) ) then
    begin
      var stop : float;
      stop := level + ( PositionEntryPrice( pos ) - level ) * _trailingStop.get2/100 ;
      if( bar <> PositionEntryBar( pos ) ) then
      begin
        if( not CoverAtStop( Bar, stop, pos, 'Trailing Stop' ) ) then
        begin
        var newLevel : float;
        newLevel := Min( PriceClose( Bar ), _activePos.getTrailingStopLevel( index ) );
        _activePos.setTrailingStopLevel( index, newLevel );
        end
      end;

      if( PositionActive( pos ) and  ( bar = barcount - 1 ) ) then
        CoverAtStop( Bar + 1, stop, pos, 'Trailing Stop' );
    end
    else if( ( bar <> PositionEntryBar(pos ) ) and ( PriceClose( Bar ) <= ( PositionEntryPrice( pos ) * ( 1 - _trailingStop.get1/100 ) ) ) ) then
    begin
      _activePos.setTrailingStopLevel( index, PriceClose( Bar ) );
    end
  end;
end;


procedure Positions.ApplyTimeBased( Bar : integer; pos : integer );
begin
  var time : integer;

  time := Bar - PositionEntryBar( pos );

  if( PositionLong( Pos ) ) then
  begin
		if ( time >= _timeBasedExit.get ) then
			SellAtMarket( Bar, Pos, 'Time Based' );

		if( PositionActive( pos ) and ( bar = barcount - 1 ) and ( ( time + 1 ) >= _timeBasedExit.get ) ) then
			SellAtMarket( Bar + 1, Pos, 'Time Based' );
  end
  else
  begin
		if ( time >= _timeBasedExit.get ) then
			CoverAtMarket( Bar, Pos, 'Time Based' );

		if( PositionActive( pos ) and ( bar = barcount - 1 ) and ( ( time + 1 ) >= _timeBasedExit.get ) ) then
			CoverAtMarket( Bar + 1, Pos, 'Time Based' );
  end;
end;

procedure Positions.ApplyBreakEven( Bar, pos, index : integer );
begin
  var entryPrice, trigger : float;
  var activated, direction : boolean;
  activated := _activePos.isBreakEvenStopActive( index );
  direction := _activePos.breakEvenStopDirection( index );
  entryPrice := PositionEntryPrice( pos );

  if( PositionLong( pos ) ) then
  begin
    if activated and direction then
    begin
      if( bar <> PositionEntryBar( pos ) ) then
        SellAtStop( Bar, entryPrice, pos, 'Break Even Stop' );

      if( PositionActive( pos ) and ( Bar = barcount - 1 ) ) then
        SellAtStop( Bar + 1, entryPrice, pos, 'Break Even Stop' );

    end
    else if(  bar <> PositionEntryBar( pos ) ) then
    begin
      trigger := entryPrice * ( 1 + _breakEvenStop.get/100 );
      if( PriceClose( Bar ) >= trigger ) then
        _activePos.activateBreakEvenStop( index, true );
    end
  end
  else
  begin
    if activated and direction then
    begin
      if( bar <> PositionEntryBar( pos ) ) then
        CoverAtStop( Bar, entryPrice, pos, 'Break Even Stop' );

      if( PositionActive( pos ) and ( Bar = barcount - 1 ) ) then
        CoverAtStop( Bar + 1, entryPrice, pos, 'Break Even Stop' );

    end
    else if(  bar <> PositionEntryBar( pos ) ) then
    begin
      trigger := entryPrice * ( 1 - _breakEvenStop.get/100 );
      if( PriceClose( Bar ) <= trigger ) then
        _activePos.activateBreakEvenStop( index, true );
    end
  end;
end;

procedure Positions.ApplyReverseBreakEven( Bar, pos, index : integer );
begin
  var entryPrice, trigger : float;
  var activated, direction : boolean;
  activated := _activePos.isBreakEvenStopActive( index );
  direction := _activePos.breakEvenStopDirection( index );
  entryPrice := PositionEntryPrice( pos );

  begin
  if( PositionLong( pos ) ) then
  begin
    if activated and ( not direction ) then
    begin
      if( bar <> PositionEntryBar( pos ) ) then
        SellAtLimit( Bar, entryPrice, pos, 'Reverse Break Even Stop' );

      if( PositionActive( pos ) and ( Bar = barcount - 1 ) ) then
        SellAtLimit( Bar + 1, entryPrice, pos, 'Reverse Break Even Stop' );

    end
    else if( bar <> PositionEntryBar( pos ) ) then
    begin
      trigger := entryPrice * ( 1 - _reverseBreakEvenStop.get/100 );
      if( PriceClose( Bar ) <= trigger ) then
        _activePos.activateBreakEvenStop( index, false );
    end
  end
  else
  begin
    if activated and (not direction ) then
    begin
      if( bar <> PositionEntryBar( pos ) ) then
        CoverAtLimit( Bar, entryPrice, pos, 'Reverse Break Even Stop' );

      if( PositionActive( pos ) and ( Bar = barcount - 1 ) ) then
        CoverAtLimit( Bar + 1, entryPrice, pos, 'Reverse Break Even Stop' );

    end
    else if( bar <> PositionEntryBar( pos ) ) then
    begin
      trigger := entryPrice * ( 1 + _reverseBreakEvenStop.get/100 );
      if( PriceClose( Bar ) >= trigger ) then
        _activePos.activateBreakEvenStop( index, false );
    end
  end
  end;
end;

// Apply stop loss
procedure Positions.ApplyStopLoss( Bar : integer; pos : integer );
begin
  var stopLossPrice : float;

  if( PositionLong( pos ) ) then
  // is it long position?
  begin
    // calculate the stop loss price
    stopLossPrice := PositionEntryPrice( pos ) * ( 1 - _stopLoss.get/100 );
    // only apply if the current bar is not the position open bar - cannot look in the future
    if( PositionEntryBar( pos ) <> Bar ) then
        // otherwise set a stop order
      SellAtStop( Bar, stopLossPrice, pos, 'Stop Loss' );
      // if the position is still active, and wer are on the last bar,
      // set a sell stop order for the next bar, so the alerts will work
    if( PositionActive( pos ) and ( Bar = barcount - 1 ) ) then
      SellAtStop( Bar + 1, stopLossPrice, pos, 'Stop Loss' );
  end
  else
  begin
    stopLossPrice := PositionEntryPrice( pos ) * ( 1 + _stopLoss.get/100 );
    if( PositionEntryBar( pos ) <> Bar ) then
      CoverAtStop( Bar, stopLossPrice, pos, 'Stop Loss' );

    if( PositionActive( pos ) and ( Bar = barcount - 1 ) ) then
      CoverAtStop( Bar + 1, stopLossPrice, pos, 'Stop Loss' );
  end;
end;

// apply profit target
procedure Positions.ApplyProfitTarget( Bar : integer; pos : integer );
begin
  var targetPrice : float;
  if( PositionLong( pos ) ) then
    // handle long positions
  begin
    // calculate target price
		targetPrice := PositionEntryPrice( pos ) * ( 1 + _profitTarget.get/100 );

		 // do not apply on the same bar as the entry bar (cannot look in the future)
		if( PositionEntryBar( pos ) <> Bar ) then
			SellAtLimit( Bar, targetPrice, pos, 'Profit Target' );
				 // else set a limit order


			// if after all these the position is still active and we are on the last bar,
			// set the possible limit price for the next bar.
			// this is needed for the alerts
		if( PositionActive( pos ) and ( bar = barcount - 1 ) ) then
			SellAtLimit( Bar + 1, targetPrice, pos, 'Profit Target' );
  end
  else
  begin
    targetPrice := PositionEntryPrice( pos ) * ( 1 - _profitTarget.get/100 );
    if PositionEntryBar( pos ) <> Bar then
      CoverAtLimit( Bar, targetPrice, pos, 'Profit Target' );

    if( PositionActive( pos ) and ( bar = barcount - 1 ) ) then
      CoverAtLimit( Bar + 1, targetPrice, pos, 'ProfitTarget' );
  end;
end;

// auto stops are processed in this order:
// - Time-Based
// - Stop Loss
// - Trailing
// - Breakeven
// - Reverse Breakeven
// - Profit Target
procedure Positions.ApplyAutoStops( Bar : integer );
begin
  var n : integer;
  var pos : integer;
    // we have to go in reverse order as when we remove a position from the _activePos, the indexes get moved down,
  for n := _activePos.count - 1 downto 0 do
  begin
    pos := _activePos.get( n );
    if _timeBasedExit.isSet then
      ApplyTimeBased( Bar, pos );
    if _stopLoss.isSet then
      ApplyStopLoss( Bar, pos );
    if _trailingStop.isSet then
      ApplyTrailing( Bar, pos, n );
    if _breakEvenStop.isSet then
      ApplyBreakEven( Bar, pos, n );
    if _reverseBreakevenStop.isSet then
      ApplyReverseBreakEven( Bar, pos, n );
    if _profitTarget.isSet then
      ApplyProfitTarget( Bar , pos );
  end;
end;

procedure Positions.InstallBreakEvenStop( Trigger : float );
begin
  _breakEvenStop.set( Trigger );
end;

procedure Positions.InstallProfitTarget( TargetLevel : float );
begin
  _profitTarget.set( TargetLevel );
end;

procedure Positions.InstallReverseBreakEvenStop( LossLevel : float );
begin
  _reverseBreakEvenStop.set( LossLevel );
end;

procedure Positions.InstallStopLoss( StopLevel : float );
begin
  _stopLoss.set( StopLevel );
end;

procedure Positions.InstallTimeBasedExit( Bars : integer );
begin
  _timeBasedExit.set( Bars );
end;

procedure Positions.InstallTrailingStop( Trigger, StopLevel : float );
begin
  _trailingStop.set( Trigger,StopLevel );
end;



procedure Positions.addPosition;
begin
  _pos.Add( LastPositionX );
  _activePos.add( _pos.Count - 1 );
end;

procedure Positions.removePosition( bar : integer; index : integer );
begin
   // don't need to remove from _pos, as closed positions stay there as closed
   // we do need it from activePos
   // this test is needed in order to implement the alert generation right
   // when we are past the last bar and sell or cover, do not remove the position
   // or this may affect the system (other buys may get triggered, because this may have been
   // the last active position
  if( bar < barcount ) then
  _activePos.remove( index );
end;


function Positions.BuyAtClose( Bar : integer; SignalName : string ) : boolean;
begin
  if( buyAtCloseFilter( bar, SignalName ) and BuyAtCloseX( Bar, _positionNameExtension.apply( SignalName ) ) )then
  begin
   // this will map the position relative to the current system to the global position
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.BuyAtLimit( Bar : integer; LimitPrice : float; SignalName : string ) : boolean;
begin
  if( buyAtLimitFilter( bar, LimitPrice, SignalName ) and BuyAtLimitX( Bar, LimitPrice, _positionNameExtension.apply( SignalName )  ) ) then
  begin
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.BuyAtMarket( Bar : integer; SignalName : string ) : boolean;
begin
  if( buyAtMarketFilter( bar, SignalName ) and BuyAtMarketX( Bar, _positionNameExtension.apply( SignalName )  ) ) then
  begin
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.BuyAtStop( Bar : integer; StopPrice : float; SignalName : string ): boolean;
begin
  if( buyAtStopFilter( bar, StopPrice, SignalName ) and BuyAtStopX( Bar, StopPrice, _positionNameExtension.apply( SignalName )  ) ) then
  begin
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.CoverAtClose( Bar, Position : integer; SignalName : string ) : boolean;
begin
  if( coverAtCloseFilter( bar, Position, SignalName ) and CoverAtCloseX( Bar, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.CoverAtLimit( Bar : integer; LimitPrice : float; Position: integer; SignalName : string ) : boolean;
begin
  if( coverAtLimitFilter( bar, LimitPrice, Position, SignalName ) and CoverAtLimitX( Bar, LimitPrice, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.CoverAtMarket( Bar, Position : integer; SignalName : string ) : boolean;
begin
  if( coverAtMarketFilter( Bar, Position, SignalName ) and CoverAtMarketX( Bar, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.CoverAtStop( Bar : integer; StopPrice: float; Position : integer; SignalName : string ) : boolean;
begin
  if( coverAtStopFilter( Bar, StopPrice, Position, SignalName ) and CoverAtStopX( Bar, StopPrice, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.CoverAtTrailingStop( Bar : integer; Stop: float; Position : integer; SignalName : string ) : boolean;
begin
  if( coverAtStopFilter( Bar, Stop, Position, SignalName ) and CoverAtTrailingStopX( Bar, Stop, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.SellAtClose( Bar, Position : integer; SignalName : string ) : boolean;
begin
  if( sellAtCloseFilter( Bar, Position, SignalName ) and SellAtCloseX( Bar, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.SellAtLimit( Bar : integer; LimitPrice : float; Position : integer; SignalName : string ) : boolean;
begin
  if( sellAtLimitFilter( Bar, LimitPrice, Position, SignalName ) and SellAtLimitX( Bar, LimitPrice, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.SellAtMarket( Bar, Position : integer; SignalName : string ) : boolean;
begin
  if( sellAtMarketFilter( Bar, Position, SignalName ) and SellAtMarketX( Bar, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.SellAtStop( Bar: integer; StopPrice: float; Position: integer; SignalName: string ): boolean;
begin
  if( sellAtStopFilter( Bar, StopPrice, Position, SignalName ) and SellAtStopX( Bar, StopPrice, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.SellAtTrailingStop( Bar: integer; Stop: float; Position: integer; SignalName: string ): boolean;
begin
  if( sellAtTrailingStopFilter( Bar, Stop, Position, SignalName ) and SellAtTrailingStopX( Bar, Stop, getPositionGlobalIndex( Position ), _positionNameExtension.apply( SignalName )  ) ) then
  begin
    removePosition( bar, Position );
    Result := true;
  end
  else
    Result := false;
end;

function Positions.ShortAtClose( Bar : integer; SignalName : string ) : boolean;
begin
  if( shortAtCloseFilter( bar, SignalName ) and ShortAtCloseX( Bar, _positionNameExtension.apply( SignalName )  ) ) then
  begin
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.ShortAtLimit( Bar: integer; LimitPrice: float; SignalName: string ): boolean;
begin
  if( shortAtLimitFilter( bar, LimitPrice, SignalName ) and ShortAtLimitX( Bar, LimitPrice, _positionNameExtension.apply( SignalName )  ) ) then
  begin
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.ShortAtMarket( Bar: integer; SignalName: string ) : boolean;
begin
  if( shortAtMarketFilter( bar, SignalName ) and ShortAtMarketX( Bar, _positionNameExtension.apply( SignalName )  ) ) then
  begin
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.ShortAtStop( Bar: integer; StopPrice: float; SignalName: string ): boolean;
begin
  if( shortAtStopFilter( bar, StopPrice, SignalName ) and ShortAtStopX( Bar, StopPrice, _positionNameExtension.apply( SignalName )  ) ) then
  begin
    addPosition;
    Result := true;
  end
  else
    Result := false;
end;

function Positions.SplitPosition( Position: integer; RetainPct: float ): integer;
begin
  Result := splitPositionX( getPositionGlobalIndex( Position ), RetainPct );
end;

function Positions.PositionBasisPrice( Position: integer ): float;
begin
  Result := PositionBasisPriceX( getPositionGlobalIndex( Position ) );
end;

procedure Positions.SetPositionSizeFixed( Value: float );
begin
  // this method only works from SimuScript
//  _positionSizeContext.fixed( Value );
  SetPositionSizeFixedX( Value  );
end;

procedure Positions.SetPositionSizePct( Value: float );
begin
  // this method only works from SimuScript
//  _positionSizeContext.pct( Value );
  SetPositionSizePctX( Value  );
end;

procedure Positions.SetPositionSizeShares( Value: integer );
begin
  // this method only works from SimuScript
//  _positionSizeContext.shares( Value );
  SetPositionSizeSharesX( Value  );
end;

procedure Positions.SetPositionSize( Size : float );
begin
  SetPositionSizeX( Size );
end;

procedure Positions.switchContext;
begin
  _positionSizeContext.switch;
end;
